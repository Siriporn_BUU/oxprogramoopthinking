
public class Board {
	private Game board ;
	private Player x ;
	private Player o ;
	private Player winner ;
	private Player current ;
	private int player ;
	private int turnCount ;
	private int row , column ;
	static char[][] Table = { { '1', '2', '3' } , { '4', '5', '6' } , { '7', '8', '9' } } ;

	Board(Player x , Player o) {
		this.x = x ;
		this.o = o ;
		current = x ;
		player = 1 ;
		turnCount = 0 ;
	}

	public int getCount() {
		turnCount += 1 ;
		return turnCount ;
	}

	public boolean isEnd() {
		if ((Table[0][0] == Table[1][1] && Table[0][0] == Table[2][2]) || Table[0][2] == Table[1][1] && Table[0][2] == Table[2][0]) {
			winner = current ;
			return true ;
		} 
		else {
			for (int line = 0; line <= 2; line++) {
				if ((Table[line][0] == Table[line][1] && Table[line][0] == Table[line][2]) || (Table[0][line] == Table[1][line] && Table[0][line] == Table[2][line])) {
					winner = current ;
					System.out.println(
							"Congratuiations!!, player" + player + "[" + current + "]" + ", YOU ARE THE WINNER!") ;
					return true ;
				}
			}
		}
		return false ;
	}

	public int getPlayer() {
		return player ;
	}

	public Player getCurrent() {
		return current ;

	}

	public void swichTurn() {
		if (player == 2) {
			player-- ;
			current = x ;
		} else {
			player++ ;
			current = o ;
		}
	}

	public int getRow(int go) {
		row = --go / 3;
		return row;
	}

	public int getColumn(int go) {
		column = --go % 3;
		return column;
	}

}
