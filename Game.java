import java.util.*;

public class Game {
	private Board board ;
	private Player x ;
	private Player o ;
	private int ro , co ;

	Game() {
		o = new Player('O') ;
		x = new Player('X') ;
		board = new Board(x , o) ;
	}

	public void play() {
		showWelcome() ;
		showTable() ;
		while (true) {
			showTurn() ;
			input() ;
			setTable() ;
			if (board.isEnd()) {
				showTable() ;
				showResult() ;
				break ;
			}
			if (board.getCount() >= 9) {
				showTable() ;
				showResult() ;
				break ;
			}
			getPlayer() ;
			showTable() ;
		}
	}

	private void showTurn() {
		System.out.print("Player " + board.getPlayer() + ", please enter the number [your] " + board.getCurrent().getName() + " :") ;
	}

	public void getPlayer() {
		board.swichTurn() ;
	}

	private void showTable() {
		System.out.println("\n") ;
		System.out.printf("\t\t %c | %c | %c\n" , board.Table[0][0] , board.Table[0][1] , board.Table[0][2]) ;
		System.out.print("\t\t---+---+---\n") ;
		System.out.printf("\t\t %c | %c | %c\n" , board.Table[1][0] , board.Table[1][1] , board.Table[1][2]) ;
		System.out.print("\t\t---+---+---\n") ;
		System.out.printf("\t\t %c | %c | %c\n" , board.Table[2][0] , board.Table[2][1] , board.Table[2][2]) ;
	}

	private void showWelcome() {
		System.out.printf("\t    Welcome to Game XO.") ;
	}

	private void input() {
		Scanner kb = new Scanner(System.in) ;
		int RC = kb.nextInt() ;
		ro = board.getRow(RC) ;
		co = board.getColumn(RC) ;
	}

	private void setTable() {
		if (board.Table[ro][co] == '1' || board.Table[ro][co] == '2' || board.Table[ro][co] == '3' || board.Table[ro][co] == '4' || board.Table[ro][co] == '5' || board.Table[ro][co] == '6' || board.Table[ro][co] == '7' || board.Table[ro][co] == '8' || board.Table[ro][co] == '9') {
			board.Table[ro][co] = board.getCurrent().getName() ;
		} 
		else {
			System.out.println("!!! ERROR !!! ,Please enter the number") ;
			showTurn() ;
			input() ;
		}
	}

	public void showResult() {
		if (board.isEnd()) {
			System.out.println("Congratuiations!!, Player " + board.getPlayer() + " [" + board.getCurrent().getName() + "] " + ", YOU ARE THE WINNER!") ;
		} 
		else {
			System.out.printf("\tHow boring, it is a draw") ;
		}

	}
}
