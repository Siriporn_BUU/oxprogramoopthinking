
public class Player {
	private char name;
	private int win;
	private int draw;
	private int lose;

	Player(char name) {
		this.name = name;
		win = 0;
		draw = 0;
		lose = 0;

	}

	public char getName() {
		return name;
	}

	public int getWin() {
		return win;
	}

	public int getDraw() {
		return draw;
	}

	public void win() {
		win++;
	}

	public void lose() {
		lose++;
	}

	public void draw() {
		draw++;
	}
}
